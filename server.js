const path = require("path");

const dotenv = require("dotenv");
const { settings } = require("./src/settings");
const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const morgan = require("morgan");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const { swaggerDocs } = require("./docs/docs");
const middlewares = require("./middlewares");
const stats = require("./src/stats/stats_routes");
const search = require("./src/search/search_routes");
const repository = require("./src/repository/repository_routes");

const app = express();

if (!process.env.NODE_ENV || process.env.NODE_ENV !== "production") {
  dotenv.config({ path: path.join(__dirname, ".env") });
}

/*************MIDDLEWARES***************/

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(fileUpload());
app.use(cors({ origin: "*" }));

/****************ROUTES*****************/

app.use("/api/owl/stats", stats);
app.use("/api/owl/search", search);
app.use("/api/owl/repository", repository);

/************DOCUMENTATION**************/

app.use("/api/owl/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/***************ERRORS******************/

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

/*************SERVER START**************/

app.listen(settings.nodePort, () => {
  console.log(`Rest API running on port ${settings.nodePort}`);
});
