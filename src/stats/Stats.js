/**
 * @typedef {Object} Stats
 * @property {String} documentId
 * @property {String} userId
 * @property {String} area
 * @property {Date} date
 */

if (!process.env.NODE_ENV || process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}
const { Client } = require("@elastic/elasticsearch");
const { settings } = require("../settings");
const client = new Client({ node: settings.elasticHost });
const indexName = settings.statsIndexName;

/**
 * @param {String} documentId
 * @param {String} userId
 * @param {String} area
 * @returns {Stats}
 */
function createStat(documentId, userId, area) {
  return { documentId, userId, area, date: Date.now() };
}

/**
 *
 * @param {String} documentId
 * @param {String} userId
 * @param {String} area
 * @returns {Promise}
 */
function add(documentId, userId, area) {
  let stat = createStat(documentId, userId, area);
  return client.index({ index: indexName, body: { ...stat } });
}

async function fetchPerDate(interval) {
  let start = "5y";
  if (interval === "month") start = "12M";
  if (interval === "week") start = "1M";
  if (interval === "day") start = "1M";
  if (interval === "hour") start = "24h";
  if (interval === "minute") start = "60m";

  let result = await client.search({
    index: indexName,
    size: 0,
    body: {
      query: {
        range: {
          date: {
            gte: `now-${start}`,
            lt: "now",
          },
        },
      },
      aggs: {
        date: {
          date_histogram: {
            field: "date",
            calendar_interval: interval,
            extended_bounds: {
              min: `now-${start}`,
              max: "now",
            },
          },
        },
      },
    },
  });

  //Clean the result
  result = { buckets: result.body.aggregations.date.buckets };

  result = result.buckets.map((value) => {
    return { key: value.key, doc_count: value.doc_count };
  });

  return result;
}

async function totalViews() {
  let result = await client.count({ index: indexName });
  result = result.body.count;
  return result;
}

async function getDocumentStats(documentId) {
  let result = await client.search({
    index: indexName,
    size: 0,
    body: {
      query: { term: { documentId } },
      aggs: {
        documentId: {
          terms: { field: "documentId", size: 50 },
          aggs: {
            area: { terms: { field: "area" } },
          },
        },
      },
    },
  });

  result = { buckets: result.body.aggregations.documentId.buckets };
  result = result.buckets.map((value) => {
    return {
      key: value.key,
      doc_count: value.doc_count,
      area: value.area.buckets,
    };
  });

  return result;
}

async function fetchPerDocument() {
  let result = await client.search({
    index: indexName,
    size: 0,
    body: {
      aggs: {
        documentId: {
          terms: { field: "documentId", size: 50 },
          aggs: {
            area: { terms: { field: "area" } },
          },
        },
      },
    },
  });

  //Clean the result
  let ids = [];
  result = { buckets: result.body.aggregations.documentId.buckets };
  result = result.buckets.map((value) => {
    ids.push(value.key);
    return {
      key: value.key,
      doc_count: value.doc_count,
      area: value.area.buckets,
    };
  });

  let documentNames = await client.mget({
    index: settings.metaIndexName,
    body: { ids },
  });

  documentNames = documentNames.body.docs;

  result = result.map((element, index) => {
    let documentInfo = documentNames[index];
    documentInfo = documentInfo._source;
    if (!documentInfo) documentInfo = { name: "Documento desconocido" };
    else documentInfo = { name: documentInfo.name };

    return { ...element, documentInfo };
  });

  //result = result.filter((element) => !element.skip);

  return result;
}

module.exports = {
  add,
  fetchPerDocument,
  totalViews,
  getDocumentStats,
  fetchPerDate,
};
