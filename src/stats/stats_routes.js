const express = require("express");
const Stats = require("./Stats");
const router = express.Router();

router.get("/total-views", async (req, res, next) => {
  try {
    const result = await Stats.totalViews();
    return res.status(200).send({ value: result });
  } catch (error) {
    next(error);
  }
});

router.get("/documents", async (req, res, next) => {
  try {
    const result = await Stats.fetchPerDocument();
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

router.get("/views-per-date", async (req, res, next) => {
  try {
    const result = await Stats.fetchPerDate(req.query.interval);
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

router.get("/documents/:id", async (req, res, next) => {
  try {
    const result = await Stats.getDocumentStats(req.params.id);
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
