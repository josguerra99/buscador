const express = require("express");
const SearchEngine = require("./SearchEngine");
const router = express.Router();

router.get("/", async (req, res, next) => {
  const page = req.query.page;
  const size = req.query.size;

  try {
    const result = await SearchEngine.search(req.query.text, page, size);
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
