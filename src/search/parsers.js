async function byParagraph(pageData) {
  //check documents https://mozilla.github.io/pdf.js/
  let render_options = {
    //replaces all occurrences of whitespace with standard spaces (0x20). The default value is `false`.
    normalizeWhitespace: true,
    //do not attempt to combine same line TextItem's. The default value is `false`.
    disableCombineTextItems: false,
  };

  const textContent = await pageData.getTextContent(render_options);

  let lastY,
    lastDIFF,
    text = "";

  let paragraphs = [];
  let oldTransform = [];
  let newparagraph = false;

  for (let item of textContent.items) {
    if (!isNullOrWhitespace(item.str)) {
      if (oldTransform.length == 0) {
        oldTransform = item.transform;
      }
      text += " " + item.str;
      newparagraph = false;
    } else {
      if (!newparagraph) {
        paragraphs.push({
          text,
          transform: oldTransform,
          page: pageData.pageIndex,
        });
        oldTransform = [];
        text = "";
      }
      newparagraph = true;
    }
  }

  if (paragraphs.length == 0) return "";
  return `${JSON.stringify(paragraphs).slice(1, -1)},`;
}

function isNullOrWhitespace(input) {
  return !input || !input.trim();
}

function byPage() {}

function document() {}

module.exports = {
  byParagraph,
};
