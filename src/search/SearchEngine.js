const fs = require("fs");
const { settings } = require("../settings");
const pdf = require("pdf-parse");
const { searchResponse } = require("../utils/common-responses");
const { Client } = require("@elastic/elasticsearch");
const parsers = require("./parsers");
const client = new Client({ node: settings.elasticHost });
const indexName = settings.mainIndexName;

async function save(documentId, documentName, path) {
  const data = await fs.promises.readFile(path);
  const pdfData = await pdf(data, { pagerender: parsers.byParagraph });
  let text = pdfData.text.trim();
  while (text.endsWith(",")) text = text.slice(0, -1).trim();
  text = `[${text}]`;
  const obj = JSON.parse(text);
  return _save(obj, documentId, documentName);
}

function isNullOrWhitespace(input) {
  return !input || !input.trim();
}

function disableVersion(id) {
  return client.updateByQuery({
    index: indexName,
    refresh: true,
    body: {
      query: { term: { documentId: id } },
      script: {
        source: `ctx._source["lastVersion"]="false"`,
      },
    },
  });
}

function _save(data, documentId, documentName) {
  var bulkItems = [];
  data.forEach((element) => {
    var action = { create: {} };
    action.create = { _index: indexName };
    var insert = {
      text: element.text,
      transform: element.transform,
      page: element.page,
      lastVersion: true,
      documentId,
      name: documentName,
    };
    if (!isNullOrWhitespace(insert.text)) {
      bulkItems.push(action);
      bulkItems.push(insert);
    }
  });

  if (bulkItems.length === 0) return null;
  return client.bulk({ body: bulkItems, refresh: true });
}

/**
 * @param {String} text
 * @param {Number} page
 * @param {Number} pageSize
 */

async function search(text, page, pageSize) {
  let bool = {};
  //bool.must_not = [{ exists: { field: "nextVersion" } }];

  if (!text) text = "";
  bool.must = [
    {
      match: { text: { query: text, fuzziness: "auto" } },
    },
    { term: { lastVersion: true } },
  ];
  bool.should = [{ match_phrase: { text: { query: text, slop: 5 } } }];

  let result = await client.search({
    index: indexName,
    from: page * pageSize,
    size: pageSize,
    body: {
      query: {
        bool,
      },
      highlight: { fields: { text: {} }, fragment_size: "300" },
    },
  });

  return searchResponse(result, (element) => {
    return {
      highlight: element.highlight.text[0],
    };
  });
}

module.exports = {
  save,
  search,
  disableVersion,
};
