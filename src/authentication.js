async function getUser(token) {
  return;
}

/**
 *
 * @param {string} token sesión del usuario
 * @param {string} requiredRole rol necesario
 */
async function isAuthorized(token, requiredRole) {
  return true;
}

module.exports = {
  isAuthorized,
};
