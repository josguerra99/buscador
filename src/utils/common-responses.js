function searchResponse(result, hitExtraMapping) {
  result = result.body.hits;
  result = {
    total: result.total.value,
    pageCount: result.hits.length,
    hits: result.hits,
  };

  result.hits = result.hits.map((element, index) => {
    const hit = { id: element._id, ...element._source };
    const extra =
      hitExtraMapping == null ? {} : hitExtraMapping(element, index);
    return { ...hit, ...extra };
  });
  return result;
}

module.exports = { searchResponse };
