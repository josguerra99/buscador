/**
 * @typedef {Object} Settings
 * @property {Number} nodePort
 * @property {string} mainIndexName
 * @property {string} metaIndexName
 * @property {string} statsIndexName Nombre del indice que contiene las estadisticas
 * @property {string} filesPath Ubicacion en la que se guardaran los archivos
 * @property {string} elasticHost Host de elastic search
 * @property {string} fileServerURL URI para abrir los archivos
 */

const fs = require("fs");
const path = require("path");

/**
 * @type {Settings}
 */
const settings = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, "../settings.json"), "utf8")
);

module.exports = { settings };
