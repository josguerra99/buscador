const express = require("express");
const Repository = require("./Repository");
const Stats = require("../stats/Stats");
const router = express.Router();
const { settings } = require("../settings");
const errMissingReq = new Error("Missing Request data");

router.post("/documents", async (req, res, next) => {
  let file = req.files.pdf;
  let name = req.body.name;
  let date = req.body.date;
  let type = req.body.type;
  let previousId = req.body.previousId;
  if (type != "interno" && type != "externo") type = null;

  if (!file || !name || !date || !type) {
    next(errMissingReq);
    return;
  }

  try {
    let result = null;

    if (previousId == null)
      result = await Repository.save(file, name, date, type);
    else
      result = await Repository.saveVersion(file, previousId, name, date, type);

    res.status(201).send(result);
  } catch (error) {
    next(error);
  }
});

router.put("/documents/:id", async (req, res, next) => {
  let body = req.body,
    data = {};

  if (body.date) data.date = body.date;
  if (body.type) data.type = body.type;
  //if (body.name) data.name = body.name;

  try {
    const result = await Repository.updateDocument(req.params.id, data);
    res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

router.get("/files/:id", async (req, res, next) => {
  const addStat =
    req.query.addStat && req.query.addStat.toLowerCase() === "true";

  const area = req.query.area;
  const user = req.query.user;

  if (!user || !area) {
    next(errMissingReq);
    return;
  }

  try {
    await Repository.getById(req.params.id);
    if (addStat) {
      await Stats.add(req.params.id, user, area);
    }
    //const file = Repository.filepath(req.params.id);
    let url = settings.fileServerURL;
    if (settings.fileServerURL.endsWith("/")) url = url.slice(0, -1);
    res.status(200).send({
      url: `${url}/${req.params.id}.pdf`,
    });
  } catch (error) {
    next(error);
  }
});

router.get("/documents/:id", async (req, res, next) => {
  try {
    let result = await Repository.getById(req.params.id);
    result = { id: result.body._id, ...result.body._source };

    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

router.get("/documents/:id/can-be-deleted", async (req, res, next) => {
  try {
    let result = await Repository.canBeUpdated(req.params.id);
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

router.delete("/documents/:id", async (req, res, next) => {
  try {
    let result = await Repository.remove(req.params.id);
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

router.get("/documents", async (req, res, next) => {
  let page = req.query.page;
  let size = req.query.size;

  if (!page || !size) {
    next(errMissingReq);
    return;
  }

  try {
    const result = await Repository.getPage(page, size, req.query.name);
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

router.get("/documents/:id/versions", async (req, res, next) => {
  try {
    const result = await Repository.getAllVersions(req.params.id);
    return res.status(200).send(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
