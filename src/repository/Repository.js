/**
 * @typedef {Object} MetadataSchema
 * @property {String} name Nombre del documento
 * @property {Date} date Fecha de aprobación
 * @property {Date} creationTime Fecha de creación en el servidor
 * @param {('interno'|'externo')} type Tipo de normativo
 * @property {Number} version Número de version
 * @property {Array<string>} previousVersions Ids de todas las versiones anteriores
 * @property {String} nextVersion Id de la siguiente version (nulo en la ultima version)
 * @property {String} originalVersion Id de la version original
 */

/**
 * @typedef {Object} UpdateDocumentSchema
 * @property {String} name
 * @property {Date} date
 * @property {('interno'|'externo')} type
 */

/**
 *
 */
if (!process.env.NODE_ENV || process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const { Client } = require("@elastic/elasticsearch");
const { settings } = require("../settings");
const path = require("path");
const client = new Client({ node: settings.elasticHost });
const SearchEngine = require("../search/SearchEngine");
const Stats = require("../stats/Stats");

const { searchResponse } = require("../utils/common-responses");
const indexName = settings.metaIndexName;

/**
 *
 * @param {string} name
 * @param {Date} date
 * @param {('interno'|'externo')} type
 * @returns {MetadataSchema}
 */
function defaultMetadata(name, date, type) {
  return {
    name,
    date,
    type,
    nextVersion: null,
    previousVersions: [],
    version: 1,
    creationTime: Date.now(),
  };
}

/**
 * @param {string} id
 * @returns {string} Path to the file
 */
function filepath(id) {
  var p = path.join(settings.filesPath, `${id}.pdf`);
  return p;
}

/**
 *
 * @param {import("@types/express-fileupload").UploadedFile} file
 * @param {string} id
 *
 * @returns {Promise}
 */
function saveFile(file, id) {
  return file.mv(filepath(id));
}

/**
 *
 * @param {string} id
 * @param {MetadataSchema} metadata
 * @returns {Promise.<import("@elastic/elasticsearch").ApiResponse>}
 */
function addVersion(id, nextVersion) {
  return client.update({
    index: indexName,
    id,
    body: {
      doc: {
        nextVersion,
      },
    },
    refresh: true,
  });
}

/**
 * @param {import("../types/common-types").MetadataSchema} metadata
 * @returns {Promise.<import("@elastic/elasticsearch").ApiResponse>}
 */
function saveInfo(metadata) {
  return client.index({
    index: indexName,
    body: { ...metadata },
    refresh: true,
  });
}

/**
 *
 * @param {string} id
 * @returns {Promise.<import("@elastic/elasticsearch").ApiResponse>}
 */
function getById(id) {
  return client.get({ index: indexName, id });
}

/**
 *
 * Saves a new pdf
 *
 * @param {import("@types/express-fileupload").UploadedFile} file
 * @param {string} name
 * @param {("interno"|"externo")} type
 * @param {Date} date
 * @returns {Promise.<import("@elastic/elasticsearch").ApiResponse>}
 */

async function save(file, name, date, type) {
  let metadata = defaultMetadata(name, date, type);

  const res = await saveInfo(metadata);
  const body = res.body;
  await saveFile(file, body._id);
  await client.update({
    index: indexName,
    id: body._id,
    body: {
      doc: {
        originalVersion: body._id,
      },
    },
    refresh: true,
  });

  await SearchEngine.save(body._id, name, filepath(body._id));

  return { id: body._id, created_at: metadata.creationTime };
}

/**
 *
 * Saves a new version of an existing
 * pdf
 *
 * @param {import("@types/express-fileupload").UploadedFile} file
 * @param {string} name
 * @param {Date} date
 * @param {('interno'|'externo')} type
 *
 * @returns {Promise.<import("@elastic/elasticsearch").ApiResponse>}
 */
async function saveVersion(file, previousId, name, date, type) {
  const res = await getById(previousId);
  let metadata = defaultMetadata(name, date, type);
  let previousMetadata = res.body._source;
  metadata.previousVersions = [
    ...previousMetadata.previousVersions,
    previousId,
  ];

  if (previousMetadata.nextVersion != null)
    throw new Error("Este documento ya contiene una versión mas nueva");

  metadata.originalVersion = previousMetadata.originalVersion;
  metadata.version = previousMetadata.version + 1;
  const res_1 = await saveInfo(metadata);
  let id = res_1.body._id;
  await addVersion(previousId, res_1.body._id);
  await saveFile(file, id);
  await SearchEngine.save(id, name, filepath(id));
  await SearchEngine.disableVersion(previousId);

  return { id, created_at: metadata.creationTime };
}

async function getAllVersions(id) {
  const res = await getById(id);

  let metadata = res.body._source;

  let result = await client.search({
    index: indexName,
    size: 255,
    body: {
      query: { term: { originalVersion: metadata.originalVersion } },
      sort: [{ creationTime: { order: "desc" } }],
    },
  });

  return searchResponse(result);
}

/**
 *
 * @param {Number} page
 * @param {Number} pageSize
 *
 * @returns {Promise}
 */
async function getPage(page, pageSize, name) {
  let bool = {};
  let sort = [];
  bool.must_not = [{ exists: { field: "nextVersion" } }];

  //if (name) bool.must = [{ match: { name: { query: name } } }];
  if (name) {
    bool.must = [
      {
        match: {
          name: {
            query: name,
            analyzer: "spanish_no_stemmer",
            fuzziness: "auto",
            operator: "and",
          },
        },
      },
    ];

    bool.should = [
      {
        match_phrase: {
          name: {
            query: name,
            analyzer: "spanish_no_stemmer",
            slop: 6,
          },
        },
      },
    ];
  } else {
    sort = [{ creationTime: { order: "desc" } }];
  }

  const result = await client.search({
    index: indexName,
    from: page * pageSize,
    size: pageSize,
    body: {
      //track_total_hits: true,
      query: {
        bool,
      },
      sort,
    },
  });

  return searchResponse(result);
}

function getCount(name) {
  let bool = {};
  bool.must_not = [{ exists: { field: "nextVersion" } }];

  if (name)
    bool.must = [
      { match_phrase: { name: { query: name, analyzer: "standard" } } },
    ];

  return client.count({
    index: indexName,
    body: {
      query: {
        bool,
      },
    },
  });
}

/**
 *
 * @param {string} documentId
 * @param {UpdateDocumentSchema} data
 * @returns {Promise}
 */
async function updateDocument(documentId, data) {
  let result = await client.update({
    index: indexName,
    id: documentId,
    refresh: true,
    body: { doc: data },
  });

  //Clean the result
  result = result.body;

  return {
    id: result._id,
    updated_at: Date.now(),
    version: result._version,
    result: result.result,
  };
}

/**
 *
 * @param {string} documentId
 * @returns {Promise}
 */
async function canBeUpdated(documentId) {
  let document = await getById(documentId);
  document = document.body._source;

  if (document.nextVersion != null)
    return {
      value: false,
      reason: "Solo se puede realizar esta accion en la ultima version",
    };
  let stats = await Stats.getDocumentStats(documentId);
  if (stats.length != 0)
    return {
      value: false,
      reason: "El documento ya tiene vistas de los usuarios finales",
    };
  return { value: true };
}

async function remove(documentId) {
  let document = await getById(documentId);
  document = document.body._source;
  const can = await canBeUpdated(documentId);
  if (!can.value) throw new Error(can.reason);

  const previousId =
    document.previousVersions[document.previousVersions.length - 1];

  if (document.previousVersions.length > 0)
    await client.update({
      index: indexName,
      id: previousId,
      body: {
        doc: {
          nextVersion: null,
        },
      },
    });

  await client.delete({ index: indexName, id: documentId, refresh: true });
  await client.deleteByQuery({
    index: settings.mainIndexName,
    refresh: true,
    body: { query: { term: { documentId } } },
  });

  return { id: documentId, deleted_at: Date.now() };
}

async function deleteDocument(documentId) {
  const document = await getById(documentId);
  //Remover la metadata
  await client.delete({ index: indexName, id: documentId, refresh: true });

  //Si no es la primera version, remover el nextversion de la version anterior
  const previousVersions = document.body.previousVersions;
  if (previousVersions && previousVersions.length > 0) {
    await client.update({
      index: indexName,
      id: previousVersions[previousVersions.length - 1],
      refresh: true,
      body: {
        doc: {
          nextVersion: null,
        },
      },
    });
  }

  //Remover todo el texto que se agrego al indice principal
  return client.deleteByQuery({
    index: indexName,
    refresh: true,
    body: { query: { match: { documentId } } },
  });
}

module.exports = {
  getById,
  save,
  saveVersion,
  filepath,
  getPage,
  getCount,
  getAllVersions,
  deleteDocument,
  updateDocument,
  remove,
  canBeUpdated,
};
