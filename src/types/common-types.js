/**
 * @typedef {Object} MetadataSchema
 * @property {String} name Nombre del documento
 * @property {Date} date Fecha de aprobación
 * @property {Date} creationTime Fecha de creación en el servidor
 * @param {('interno'|'externo')} type Tipo de normativo
 * @property {Number} version Número de version
 * @property {Array<string>} previousVersions Ids de todas las versiones anteriores
 * @property {String} nextVersion Id de la siguiente version (nulo en la ultima version)
 * @property {String} originalVersion Id de la version original
 */
