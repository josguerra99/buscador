const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const path = require("path");

const swaggerOptions = {
  swaggerDefinition: {
    components: {},
    info: {
      title: "Owl-backend API",
      description: "End points from Owl Restful API",
      version: "1.0.0",
    },
    servers: ["http://localhost:3000"],
  },
  apis: [path.join(__dirname, "./**/*.yaml")],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
module.exports = { swaggerDocs };
